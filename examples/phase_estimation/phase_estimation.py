from quant.phase_estimation import phase_estimation, generator_unitary_matrix
from quant import State, X, H
import numpy as np
import cmath

t = 10
n = 5

U = generator_unitary_matrix(2 ** n)
# print(np.linalg.eig(U))
# print(np.linalg.eig(U)[1][:, 0])
eig_vector = np.linalg.eig(U)[1][:, 0]
eig_value = np.linalg.eig(U)[0][0]
# print(eig_value)
# print(cmath.phase(eig_value) / (2 * np.pi))
# print(eig_vector)


state = State()
full_vector_t = np.zeros(2 ** t, dtype=complex)
full_vector_t[0] = 1.0
full_vector_n = eig_vector
full_vector = np.kron(full_vector_t, full_vector_n)
# print(full_vector)
state.full_tensor = np.reshape(full_vector, [2] * (t + n))
state.N = t + n
phase_estimation(state, t, n, U)
# print(state.return_full_vector())
# print(np.abs(state.return_full_vector().max()) ** 2)

i_max = 0
max_overlap = 0
for i in range(2 ** t):
    full_vector_left_t = np.zeros(2 ** t, dtype=complex)
    full_vector_left_t[i] = 1.0
    full_vector_left = np.kron(full_vector_left_t, full_vector_n)
    overlap = np.dot(full_vector_left.conjugate(), state.return_full_vector())
    # print(i, np.abs(overlap) ** 2)
    if np.abs(overlap) ** 2 > max_overlap:
        i_max = i
        max_overlap = np.abs(overlap) ** 2
# print(i_max, max_overlap)
real_phase = cmath.phase(eig_value) / (2 * np.pi)
if real_phase < 0:
    real_phase = 1 + real_phase
estimate_phase = i_max / (2 ** t)
print('Real phase = ', real_phase)
print('Estimate phase = ', estimate_phase)

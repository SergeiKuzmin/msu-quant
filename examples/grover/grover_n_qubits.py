from quant import grover_n_qubits
from quant import State, X, H
import numpy as np

n = 5
answer_grover = ['11011', '11111']
M = len(answer_grover)
if not all(np.array(list(map(len, answer_grover))) == n):
    raise RuntimeError('Incorrect of len answer_grover')

state = State()
nums_of_iterations = int(np.pi * np.sqrt((2 ** n) / M) / 4)
print(nums_of_iterations)
grover_n_qubits(state, nums_of_iterations, answer_grover, n)
print(state.return_full_vector())
vector = list(np.abs(state.return_full_vector()) ** 2)

list_of_indexes = []
for i in range(M):
    ind_max = 0
    value_max = vector[ind_max]
    for j in range(len(vector)):
        if vector[j] > value_max:
            ind_max = j
            value_max = vector[j]
    list_of_indexes.append(ind_max)
    print(np.array(vector).max())
    vector[ind_max] = 0.0
print(list(map(lambda x: bin(x)[2:].zfill(3 * n - 2)[0:n], list_of_indexes)))

import numpy as np
from quant import grover_2_qubits
from quant import State, X, H

n = 2
state = State()
answer_grover = ['11']
M = len(answer_grover)
if not all(np.array(list(map(len, answer_grover))) == n):
    raise RuntimeError('Incorrect of len answer_grover')
nums_of_iterations = int(np.pi * np.sqrt((2 ** n) / M) / 4)
grover_2_qubits(state, nums_of_iterations, answer_grover)
print(state.return_full_vector())
vector = list(np.abs(state.return_full_vector()))

list_of_indexes = []
for i in range(M):
    ind_max = 0
    value_max = vector[ind_max]
    for j in range(len(vector)):
        if vector[j] > value_max:
            ind_max = j
            value_max = vector[j]
    list_of_indexes.append(ind_max)
    print(np.array(vector).max())
    vector[ind_max] = 0.0
print(list(map(lambda x: bin(x)[2:].zfill(3)[0:n], list_of_indexes)))

from quant import grover_n_qubits
from quant import State, X, H
import numpy as np

n = 5
answer_grover = ['11011', '11111']
M = len(answer_grover)
if not all(np.array(list(map(len, answer_grover))) == n):
    raise RuntimeError('Incorrect of len answer_grover')

state = State()
nums_of_iterations = int(np.pi * np.sqrt((2 ** n) / M) / 4)
grover_n_qubits(state, nums_of_iterations, answer_grover, n)
print(state.return_full_vector())
vector = list(np.abs(state.return_full_vector()) ** 2)

theta = 2 * np.arccos(np.sqrt((2 ** n - M) / (2 ** n)))
amps = (1 / np.sqrt(M)) * np.sin(((2 * nums_of_iterations + 1) / 2) * theta)
print(amps)
print(np.abs(state.return_full_vector()).max())

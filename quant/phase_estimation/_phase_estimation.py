import numpy as np
from quant.gate import X, H, CX, CX_reverse, TOFFOLI, CU, R_K_C, SWAP


def phase_estimation(state, t, n, u_target):
    for i in range(0, t, 1):
        state.one_qubit_gate(H(), i)
    for i in range(t - 1, -1, -1):
        for j in range(2 ** (t - i - 1)):
            state.n_qubit_gate(CU(n, u_target), [i] + list(np.arange(t, t + n, 1)))
    QFT_inverse(state, t)


def QFT_inverse(state, t):
    for i in range(int(t / 2)):
        state.two_qubit_gate(SWAP(), i, t - i - 1)
    state.one_qubit_gate(H(), t - 1)
    for i in range(t - 2, -1, -1):
        for j in range(t - i, 1, -1):
            state.two_qubit_gate(R_K_C(j).T.conjugate(), i, i + j - 1)
        state.one_qubit_gate(H(), i)


def QFT(state, t):
    for i in range(0, t - 1, 1):
        state.one_qubit_gate(H(), i)
        for j in range(2, t - i + 1, 1):
            state.two_qubit_gate(R_K_C(j), i, i + j - 1)
    state.one_qubit_gate(H(), t - 1)
    for i in range(int(t / 2)):
        state.two_qubit_gate(SWAP(), i, t - i - 1)

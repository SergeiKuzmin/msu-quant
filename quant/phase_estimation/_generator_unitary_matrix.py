import numpy as np


def generator_unitary_matrix(n):
    z = (np.random.randn(n, n) + 1j * np.random.randn(n, n))/np.sqrt(2.0)
    q, r = np.linalg.qr(z)
    ph = generator_diagonal_matrix(r, n)
    u = np.dot(q, ph)
    return u


def generator_diagonal_matrix(r, n):
    ph = np.random.randn(n, n) + 1j * np.random.randn(n, n)
    for i in range(n):
        for j in range(n):
            ph[i][j] = 0
    for i in range(n):
        ph[i][i] = r[i][i]/abs(r[i][i])
    return ph

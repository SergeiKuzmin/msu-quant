from quant.gate import X, H, CX, TOFFOLI


def grover_2_qubits(state, nums_of_iterations, answer_grover, n=2):
    state.all_zeros_state(n + 1)
    state.one_qubit_gate(X(), 2)
    for i in range(3):
        state.one_qubit_gate(H(), i)
    for i in range(nums_of_iterations):
        grover_iteration(state, answer_grover, n)
    state.one_qubit_gate(H(), 2)


def grover_iteration(state, answer_grover, n=2):
    for answer in answer_grover:
        orakul(state, answer)

    state.one_qubit_gate(H(), 0)
    state.one_qubit_gate(H(), 1)

    # Diffusion
    state.one_qubit_gate(X(), 0)
    state.one_qubit_gate(X(), 1)
    state.one_qubit_gate(H(), 1)
    state.two_qubit_gate(CX(), 0, 1)
    state.one_qubit_gate(H(), 1)
    state.one_qubit_gate(X(), 0)
    state.one_qubit_gate(X(), 1)

    state.one_qubit_gate(H(), 0)
    state.one_qubit_gate(H(), 1)


def orakul(state, answer, n=2):
    for i in range(len(answer)):
        if answer[i] == '0':
            state.one_qubit_gate(X(), i)
    state.three_qubit_gate(TOFFOLI(), 0, 1, 2)
    for i in range(len(answer)):
        if answer[i] == '0':
            state.one_qubit_gate(X(), i)

from quant.gate import X, H, CX, CX_reverse, TOFFOLI


def grover_n_qubits(state, nums_of_iterations, answer_grover, n):
    state.all_zeros_state(3 * n - 2)
    state.one_qubit_gate(X(), 3 * n - 3)
    state.one_qubit_gate(H(), 3 * n - 3)
    for i in range(n):
        state.one_qubit_gate(H(), i)
    for i in range(nums_of_iterations):
        grover_iteration(state, answer_grover, n)
    state.one_qubit_gate(H(), 3 * n - 3)


def grover_iteration(state, answer_grover, n):
    for answer in answer_grover:
        orakul(state, answer, n)
    for i in range(n):
        state.one_qubit_gate(H(), i)
    for i in range(n):
        state.one_qubit_gate(X(), i)
    state.one_qubit_gate(H(), n - 1)

    state.three_qubit_gate(TOFFOLI(), 0, 1, 2 * n - 1)
    for i in range(0, n - 3, 1):
        state.three_qubit_gate(TOFFOLI(), i + 2, 2 * n - 1 + i, 2 * n + i)
    state.two_qubit_gate(CX_reverse(), n - 1, 3 * n - 4)
    for i in range(n - 4, -1, -1):
        state.three_qubit_gate(TOFFOLI(), i + 2, 2 * n - 1 + i, 2 * n + i)
    state.three_qubit_gate(TOFFOLI(), 0, 1, 2 * n - 1)

    state.one_qubit_gate(H(), n - 1)
    for i in range(n):
        state.one_qubit_gate(X(), i)
    for i in range(n):
        state.one_qubit_gate(H(), i)


def orakul(state, answer, n):
    for i in range(len(answer)):
        if answer[i] == '0':
            state.one_qubit_gate(X(), i)

    state.three_qubit_gate(TOFFOLI(), 0, 1, n)
    for i in range(0, n - 2, 1):
        state.three_qubit_gate(TOFFOLI(), i + 2, n + i, n + i + 1)
    state.two_qubit_gate(CX(), 2 * n - 2, 3 * n - 3)
    for i in range(n - 3, -1, -1):
        state.three_qubit_gate(TOFFOLI(), i + 2, n + i, n + i + 1)
    state.three_qubit_gate(TOFFOLI(), 0, 1, n)

    for i in range(len(answer)):
        if answer[i] == '0':
            state.one_qubit_gate(X(), i)

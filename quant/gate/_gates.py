import numpy as np


def I():
    return np.array([[1, 0],
                     [0, 1]], dtype=complex)


def X():
    return np.array([[0, 1],
                     [1, 0]], dtype=complex)


def Y():
    return np.array([[0, -1j],
                     [1j, 0]], dtype=complex)


def Z():
    return np.array([[1, 0],
                     [0, -1]], dtype=complex)


def H():
    return (1 / np.sqrt(2)) * np.array([[1, 1],
                                        [1, -1]], dtype=complex)


def Rn(alpha, phi, theta):
    nx = np.sin(alpha) * np.cos(phi)
    ny = np.sin(alpha) * np.sin(phi)
    nz = np.cos(alpha)
    return np.cos(theta / 2) * I() - 1j * np.sin(theta / 2) * (nx * X() + ny * Y() + nz * Z())


def Rn_random():
    alpha = np.pi * np.random.rand()
    phi = 2 * np.pi * np.random.rand()
    theta = 2 * np.pi * np.random.rand()
    return Rn(alpha, phi, theta)


def CX():
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 0, 1],
                     [0, 0, 1, 0]], dtype=complex)


def CX_reverse():
    return np.array([[1, 0, 0, 0],
                     [0, 0, 0, 1],
                     [0, 0, 1, 0],
                     [0, 1, 0, 0]], dtype=complex)


def CY():
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 0, -1j],
                     [0, 0, 1j, 0]], dtype=complex)


def CZ():
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, -1]], dtype=complex)


def SWAP():
    return np.array([[1, 0, 0, 0],
                     [0, 0, 1, 0],
                     [0, 1, 0, 0],
                     [0, 0, 0, 1]], dtype=complex)


def TOFFOLI():
    return np.array([[1, 0, 0, 0, 0, 0, 0, 0],
                     [0, 1, 0, 0, 0, 0, 0, 0],
                     [0, 0, 1, 0, 0, 0, 0, 0],
                     [0, 0, 0, 1, 0, 0, 0, 0],
                     [0, 0, 0, 0, 1, 0, 0, 0],
                     [0, 0, 0, 0, 0, 1, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 1],
                     [0, 0, 0, 0, 0, 0, 1, 0]], dtype=complex)


def CU(n_qubits, u_target):
    u = np.eye(2 ** (n_qubits + 1), dtype=complex)
    u[2 ** n_qubits:, 2 ** n_qubits:] = u_target
    # print(u)
    return u


def R_K_C(k):
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, np.exp(2 * np.pi * 1j / (2 ** k))]], dtype=complex)


def QFT(n):
    N = 2 ** n
    qft = []
    for j in range(N):
        qft_row = []
        for k in range(N):
            qft_row.append(np.exp(2 * np.pi * 1j * j * k / N))
        qft.append(qft_row)
    qft = np.array(qft)
    qft = qft / np.sqrt(N)
    return qft

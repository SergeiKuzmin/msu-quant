import numpy as np
from quant.state._state_base import StateBase


class StateVectorStr(StateBase):
    def __init__(self):
        self.N = None
        self.len = None
        self.phys_ind = []
        self.full_vector = None

    def all_zeros_state(self, n):
        self.N = n
        self.len = 2 ** self.N
        self.phys_ind = []
        self.full_vector = np.zeros(2 ** self.N, dtype=complex)
        self.full_vector[0] = 1.0
        for i in range(n):
            self.phys_ind.append(2)

    def return_full_vector(self):
        return self.full_vector

    def one_qubit_gate(self, u, k):
        for i in range(0, 2 ** (self.N - 1), 1):
            bin_format_str = bin(i)[2:].zfill(self.N - 1)
            ind_list = []
            b_list = [0, 0]
            for j in range(2):
                ind_list.append(int(bin_format_str[0:k] + str(j) + bin_format_str[k:], 2))
            for j in range(2):
                for l in range(2):
                    b_list[j] += u[j, l] * self.full_vector[ind_list[l]]
            for j in range(2):
                self.full_vector[ind_list[j]] = b_list[j]

    def two_qubit_gate(self, u, k, l):
        u = np.reshape(u, [4, 4])
        for i in range(0, 2 ** (self.N - 2), 1):
            bin_format_str = bin(i)[2:].zfill(self.N - 2)
            if (self.N - 2) == 0:
                bin_format_str = ''
            ind_list = []
            b_list = [0, 0, 0, 0]
            for j in range(4):
                bin_format_j = bin(j)[2:].zfill(2)
                j_1 = int(bin_format_j[0])
                j_2 = int(bin_format_j[1])
                ind_list.append(int(bin_format_str[0:k] + str(j_1) + bin_format_str[k:l - 1] + str(j_2) +
                                    bin_format_str[l - 1:], 2))
            for j in range(4):
                for p in range(4):
                    b_list[j] += u[j, p] * self.full_vector[ind_list[p]]

            for j in range(4):
                self.full_vector[ind_list[j]] = b_list[j]

    def three_qubit_gate(self, u, k, l, m):
        u = np.reshape(u, [8, 8])
        for i in range(0, 2 ** (self.N - 3), 1):
            bin_format_str = bin(i)[2:].zfill(self.N - 3)
            if (self.N - 3) == 0:
                bin_format_str = ''
            ind_list = []
            b_list = [0, 0, 0, 0, 0, 0, 0, 0]
            for j in range(8):
                bin_format_j = bin(j)[2:].zfill(3)
                j_1 = int(bin_format_j[0])
                j_2 = int(bin_format_j[1])
                j_3 = int(bin_format_j[2])
                ind_list.append(int(bin_format_str[0:k] + str(j_1) + bin_format_str[k:l - 1] + str(j_2) +
                                    bin_format_str[l - 1:m - 2] + str(j_3) + bin_format_str[m - 2:], 2))

            for j in range(8):
                for p in range(8):
                    b_list[j] += u[j, p] * self.full_vector[ind_list[p]]

            for j in range(8):
                self.full_vector[ind_list[j]] = b_list[j]

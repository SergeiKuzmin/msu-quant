from abc import ABC


class StateBase(ABC):
    """ Base class for states"""
    def all_zeros_state(self, n):
        """Creates n qubits state |0>|0> ... |0>"""

    def one_qubit_gate(self, u, k):
        """Applies gate u on one qubit with k index"""

    def two_qubit_gate(self, u, k, l):
        """Applies gate u on two qubits with k and l indexes"""

    def three_qubit_gate(self, u, k, l, m):
        """Applies gate u on three qubits with k, l and m indexes"""

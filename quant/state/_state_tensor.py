import numpy as np
from quant.state._state_base import StateBase


class State(StateBase):
    def __init__(self):
        self.N = None
        self.phys_ind = []
        self.full_tensor = None

    def all_zeros_state(self, n):
        self.N = n
        self.phys_ind = []
        state_qubit = np.reshape(np.array([1, 0], dtype=complex), [1, 2, 1])
        self.full_tensor = state_qubit
        self.phys_ind.append(2)
        for i in range(1, n, 1):
            self.full_tensor = np.tensordot(self.full_tensor, state_qubit, axes=([-1], [0]))
            self.phys_ind.append(2)
        self.full_tensor = self.full_tensor.reshape(self.phys_ind)

    def return_full_tensor(self):
        return self.full_tensor

    def return_full_vector(self):
        return self.full_tensor.reshape(-1)

    def one_qubit_gate(self, u, k):
        self.full_tensor = np.tensordot(u, self.full_tensor, axes=([1], [k]))
        self.full_tensor = np.transpose(self.full_tensor, axes=tuple(list(np.arange(1, k + 1, 1)) + [0] +
                                                                     list(np.arange(k + 1, self.N, 1))))

    def two_qubit_gate(self, u, k, l):
        u = np.reshape(u, [2, 2, 2, 2])
        self.full_tensor = np.tensordot(u, self.full_tensor, axes=([2, 3], [k, l]))
        self.full_tensor = np.transpose(self.full_tensor, axes=tuple(list(np.arange(2, k + 2, 1)) + [0] +
                                                                     list(np.arange(k + 2, l + 1, 1)) + [1] +
                                                                     list(np.arange(l + 1, self.N))))

    def three_qubit_gate(self, u, k, l, m):
        u = np.reshape(u, [2, 2, 2, 2, 2, 2])
        self.full_tensor = np.tensordot(u, self.full_tensor, axes=([3, 4, 5], [k, l, m]))
        self.full_tensor = np.transpose(self.full_tensor, axes=tuple(list(np.arange(3, k + 3, 1)) + [0] +
                                                                     list(np.arange(k + 3, l + 2, 1)) + [1] +
                                                                     list(np.arange(l + 2, m + 1, 1)) + [2] +
                                                                     list(np.arange(m + 1, self.N))))

    def n_qubit_gate(self, u, list_of_index):
        n = len(list_of_index)
        u = np.reshape(u, [2] * 2 * n)
        list_index_u = [n + i for i in range(0, n, 1)]
        self.full_tensor = np.tensordot(u, self.full_tensor, axes=(list_index_u, list_of_index))
        axes_list = list(np.arange(n, list_of_index[0] + n, 1)) + [0]
        for i in range(0, n - 1, 1):
            axes_list += list(np.arange(list_of_index[i] + n - i, list_of_index[i + 1] + n - i - 1, 1)) + [i + 1]
        axes_list += list(np.arange(list_of_index[-1] + 1, self.N))
        self.full_tensor = np.transpose(self.full_tensor, axes=tuple(axes_list))

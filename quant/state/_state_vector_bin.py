import numpy as np
from quant.state._state_base import StateBase


class StateVectorBin(StateBase):
    def __init__(self):
        self.N = None
        self.len = None
        self.phys_ind = []
        self.full_vector = None

    def all_zeros_state(self, n):
        self.N = n
        self.len = 2 ** self.N
        self.phys_ind = []
        self.full_vector = np.zeros(2 ** self.N, dtype=complex)
        self.full_vector[0] = 1.0
        for i in range(n):
            self.phys_ind.append(2)

    def return_full_vector(self):
        return self.full_vector

    def one_qubit_gate(self, u, k):
        k_new = self.N - k - 1
        fl_array = np.ones(2 ** self.N, dtype=int)
        for i in range(0, 2 ** self.N, 1):
            ind_list = []
            b_list = [0, 0]
            for j in range(2):
                index = (i & (~(1 << k_new))) ^ (j * (1 << k_new))
                ind_list.append(index)
            for j in range(2):
                for l in range(2):
                    b_list[j] += u[j, l] * self.full_vector[ind_list[l]]
            for j in range(2):
                if fl_array[ind_list[j]] == 1:
                    self.full_vector[ind_list[j]] = b_list[j]
                    fl_array[ind_list[j]] = 0

    def two_qubit_gate(self, u, k, l):
        k_new = self.N - k - 1
        l_new = self.N - l - 1
        u = np.reshape(u, [4, 4])
        fl_array = np.ones(2 ** self.N, dtype=int)
        for i in range(0, 2 ** self.N, 1):
            ind_list = []
            b_list = [0, 0, 0, 0]
            for j in range(4):
                j_1 = j // 2
                j_2 = j % 2
                index = (i & (~(1 << k_new))) ^ (j_1 * (1 << k_new))
                index = (index & (~(1 << l_new))) ^ (j_2 * (1 << l_new))
                ind_list.append(index)
            for j in range(4):
                for p in range(4):
                    b_list[j] += u[j, p] * self.full_vector[ind_list[p]]

            for j in range(4):
                if fl_array[ind_list[j]] == 1:
                    self.full_vector[ind_list[j]] = b_list[j]
                    fl_array[ind_list[j]] = 0

    def three_qubit_gate(self, u, k, l, m):
        k_new = self.N - k - 1
        l_new = self.N - l - 1
        m_new = self.N - m - 1
        u = np.reshape(u, [8, 8])
        fl_array = np.ones(2 ** self.N, dtype=int)
        for i in range(0, 2 ** self.N, 1):
            ind_list = []
            b_list = [0, 0, 0, 0, 0, 0, 0, 0]
            for j in range(8):
                j_1 = j // 4
                j_2 = (j - 4 * j_1) // 2
                j_3 = (j - 4 * j_1 - 2 * j_2) % 2
                index = (i & (~(1 << k_new))) ^ (j_1 * (1 << k_new))
                index = (index & (~(1 << l_new))) ^ (j_2 * (1 << l_new))
                index = (index & (~(1 << m_new))) ^ (j_3 * (1 << m_new))
                ind_list.append(index)

            for j in range(8):
                for p in range(8):
                    b_list[j] += u[j, p] * self.full_vector[ind_list[p]]

            for j in range(8):
                if fl_array[ind_list[j]] == 1:
                    self.full_vector[ind_list[j]] = b_list[j]
                    fl_array[ind_list[j]] = 0

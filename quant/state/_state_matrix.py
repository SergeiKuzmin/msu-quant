import numpy as np
from quant.state._state_base import StateBase


class StateMatrix(StateBase):
    def __init__(self):
        self.N = None
        self.len = None
        self.phys_ind = []
        self.full_vector = None

    def all_zeros_state(self, n):
        self.N = n
        self.len = 2 ** self.N
        self.phys_ind = []
        self.full_vector = np.zeros(2 ** self.N, dtype=complex)
        self.full_vector[0] = 1.0
        for i in range(n):
            self.phys_ind.append(2)

    def return_full_vector(self):
        return self.full_vector

    def one_qubit_gate(self, u, k):
        state_tensor = self.full_vector.reshape(self.phys_ind)
        state_tensor = np.transpose(state_tensor, axes=tuple([k] + list(np.arange(0, k, 1)) +
                                                             list(np.arange(k + 1, self.N, 1))))
        state_matrix = state_tensor.reshape(2, 2 ** (self.N - 1))
        new_state_matrix = np.dot(u, state_matrix)
        new_state_tensor = new_state_matrix.reshape(self.phys_ind)
        new_state_tensor = np.transpose(new_state_tensor, axes=tuple(list(np.arange(1, k + 1, 1)) + [0] +
                                                                     list(np.arange(k + 1, self.N, 1))))
        self.full_vector = new_state_tensor.reshape(-1)

    def two_qubit_gate(self, u, k, l):
        state_tensor = self.full_vector.reshape(self.phys_ind)
        state_tensor = np.transpose(state_tensor, axes=tuple([k, l] + list(np.arange(0, k, 1)) +
                                                             list(np.arange(k + 1, l, 1)) +
                                                             list(np.arange(l + 1, self.N, 1))))
        state_matrix = state_tensor.reshape(4, 2 ** (self.N - 2))
        new_state_matrix = np.dot(u, state_matrix)
        new_state_tensor = new_state_matrix.reshape(self.phys_ind)
        new_state_tensor = np.transpose(new_state_tensor, axes=tuple(list(np.arange(2, k + 2, 1)) + [0] +
                                                                     list(np.arange(k + 2, l + 1, 1)) + [1] +
                                                                     list(np.arange(l + 1, self.N, 1))))
        self.full_vector = new_state_tensor.reshape(-1)

    def three_qubit_gate(self, u, k, l, m):
        state_tensor = self.full_vector.reshape(self.phys_ind)
        state_tensor = np.transpose(state_tensor, axes=tuple([k, l, m] + list(np.arange(0, k, 1)) +
                                                             list(np.arange(k + 1, l, 1)) +
                                                             list(np.arange(l + 1, m, 1)) +
                                                             list(np.arange(m + 1, self.N, 1))))
        state_matrix = state_tensor.reshape(8, 2 ** (self.N - 3))
        new_state_matrix = np.dot(u, state_matrix)
        new_state_tensor = new_state_matrix.reshape(self.phys_ind)
        new_state_tensor = np.transpose(new_state_tensor, axes=tuple(list(np.arange(3, k + 3, 1)) + [0] +
                                                                     list(np.arange(k + 3, l + 2, 1)) + [1] +
                                                                     list(np.arange(l + 2, m + 1, 1)) + [2] +
                                                                     list(np.arange(m + 1, self.N, 1))))
        self.full_vector = new_state_tensor.reshape(-1)

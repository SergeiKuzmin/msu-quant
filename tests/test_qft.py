import numpy as np
from quant.phase_estimation import QFT, QFT_inverse
import quant.gate as gate
from quant import State, X, H, CU, CX
import copy

t = 5
state = State()
state.all_zeros_state(t)
state.one_qubit_gate(X(), 4)
state.one_qubit_gate(X(), 3)
state.one_qubit_gate(X(), 2)
state.one_qubit_gate(H(), 1)
state.one_qubit_gate(X(), 0)
state_tmp = copy.deepcopy(state)
print(np.sum(np.abs(state.return_full_vector() - state_tmp.return_full_vector()) ** 2))
QFT(state, t)
QFT_inverse(state, t)
print(np.sum(np.abs(state.return_full_vector() - state_tmp.return_full_vector()) ** 2))

# n = 2
# state = State()
# state.all_zeros_state(n)
# state.one_qubit_gate(H(), 0)
# # state.two_qubit_gate(CX(), 0, 1)
# state.n_qubit_gate(CU(n - 1, X()), [0, 1])
# print(state.return_full_vector())

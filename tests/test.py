import numpy as np
from quant import State, X, H, CX, TOFFOLI

n = 3
state = State()
state.all_zeros_state(3)
print(state.full_tensor.shape)
print(state.phys_ind)
print(state.return_full_vector())
state.one_qubit_gate(H(), 1)
state.two_qubit_gate(CX(), 1, 2)
print(state.return_full_vector())

n = 3
state = State()
state.all_zeros_state(n)
print(state.full_tensor.shape)
state.one_qubit_gate(X(), 0)
state.one_qubit_gate(X(), 1)
state.one_qubit_gate(X(), 2)
state.three_qubit_gate(TOFFOLI(), 0, 1, 2)
print(state.return_full_vector())
